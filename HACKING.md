# Hacking on Camera

## Technologies

- Build system: [Meson](https://mesonbuild.com/)
- Programming language: [Vala](https://wiki.gnome.org/Projects/Vala/Tutorial), a compiled programming language that provides excellent compatibility with GObject libraries, such as GTK. API documentation can be found at <https://valadoc.org>.
- GUI toolkit: [GTK](https://www.gtk.org/).
- Recommended IDE: GNOME Builder. [You can install it from Flathub](https://flathub.org/apps/details/org.gnome.Builder).

Camera functionality is handled by [libaperture](https://gitlab.gnome.org/jwestman/libaperture), which is developed alongside the Camera app.


## Design

Camera is based on [mockups by the GNOME Design Team](https://gitlab.gnome.org/Teams/Design/app-mockups/tree/master/camera). Please follow them, if applicable, when changing the UI.

I find it easier to edit UI files manually. There is a graphical editor, [Glade](https://flathub.org/apps/details/org.gnome.Glade), but it sometimes messes things up.


## Files

- `build-aux/meson/postinstall.py` -- The post-install script for the build process
- `data` -- Application files
  - `icons` -- The app icons, as well as some non-standard symbolic icons used in the app
   - `screenshots` -- The screenshots used in the AppStream metadata
   - `ui` -- [GtkBuilder](https://developer.gnome.org/gtk3/stable/GtkBuilder.html) UI definitions
- `po` -- Contains translations. Don't touch any files in here except `meson.build`, if the build system needs to be tweaked; or `POTFILES`, which is a list of files that contain strings that need to be translated.
- `src` -- The application source code
  - `components` -- Miscellaneous UI widgets
    - `gallery.vala` -- The gallery view
    - `gallery-button.vala` -- The button next to the shutter which opens the gallery view
    - `gallery-page.vala` -- A page (other than the viewfinder) in the gallery view, for displaying a photo or video that was recently taken
    - `shutter-button.vala` -- The shutter button.
    - `tween.vala` -- A helper class for animating custom drawing or layouts for widgets.
  - `containers` -- Specialized [GtkContainer](https://developer.gnome.org/gtk3/stable/GtkContainer.html) subclasses for handling the adaptive layout.
    - `controls-container.vala` -- Lays out the camera control widgets, either horizontally or vertically, and may hide some of them based on its position.
    - `controls-overlay.vala` -- Contains three ControlsContainer widgets and switches between them based on the available space.
    - `layer-container.vala` -- A simple overlay container that stacks all of its child widgets in the same space.
    - `utils.vala` -- Contains a utility function
  - `build-info.vala` -- Contains declarations for a few variables that are defined at compile time, so the program knows its own version number.
  - `controls.vala` -- Handles the logic for one set of camera controls (remember, there are three such sets: sidebar, bottom, and top).
  - `controls-group.vala` -- Syncs multiple Controls widgets
  - `main.vala` -- The application entrypoint, which sets up the GtkApplication and runs it
  - `utils.vala` -- Contains a few utility functions and classes, mostly related to drawing
  - `window.vala` -- The source code for the main application window
- `net.flyingpimonster.Camera.json` -- The flatpak manifest for developing Camera
