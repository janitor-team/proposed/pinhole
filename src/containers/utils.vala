/* utils.vala
 *
 * Copyright 2020 James Westman <james@flyingpimonster.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


namespace Camera {
    /**
     * Used in container implementations, for the forall_internal functions.
     * A foreach loop won't work here because the list might be modified during
     * iteration, which would crash.
     */
    public void foreach_widget(List<Gtk.Widget> widgets, Gtk.Callback callback) {
        while (widgets != null) {
            var widget = widgets.data;
            widgets = widgets.next;
            callback(widget);
        }
    }
}   
