/* controls-overlay.vala
 *
 * Copyright 2020 James Westman <james@flyingpimonster.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */


// Adapted from https://gitlab.gnome.org/GNOME/gnome-games/-/blob/d56cdd05a055d6e2a878a4735a436dc3993ecfe7/src/ui/titlebar-box.vala


/**
 * An overlay that contains three #CameraControls widgets: a sidebar, a bottom
 * bar, and a top bar. They will be hidden and revealed based on the available
 * space.
 */
public class Camera.ControlsOverlay : Gtk.Container {
    public ControlsGroup group { get; set; }

    public bool sidebar_showing { get; private set; }

    public int transition_duration { get; set; default=250; }


    private Controls bottom;
    private Controls side;
    private Controls top;

    /**
     * If the overlay is narrower than this, the bottom/top bars will always be
     * shown even if there is room for the sidebar.
     */
    private const int MOBILE_LAYOUT_THRESHOLD = 400;


    private Tween tween;
    private enum Tweens {
        TRANSITION,
        TOP_TRANSITION,
        LAST_TWEEN
    }


    construct {
        set_has_window(false);

        tween = new Tween(this, Tweens.LAST_TWEEN);
        tween.resize = true;
        tween.start(Tweens.TRANSITION, 1, 0);

        side = new Controls(SIDEBAR);
        bottom = new Controls(BOTTOM);
        top = new Controls(TOP);

        side.set_parent(this);
        bottom.set_parent(this);
        top.set_parent(this);

        group = new ControlsGroup();
        group.add_controls(side);
        group.add_controls(bottom);
        group.add_controls(top);

        bottom.notify["narrow"].connect(on_layout_update);
        notify["sidebar-showing"].connect(on_layout_update);
        on_layout_update();
    }


    protected override void get_preferred_width(out int min, out int nat) {
        int bottom_min = 0, bottom_nat = 0;
        int top_min = 0, top_nat = 0;

        if (bottom != null) {
            bottom.get_preferred_width(out bottom_min, out bottom_nat);
        }
        if (top != null) {
            top.get_preferred_width(out top_min, out top_nat);
        }

        min = int.max(top_min, bottom_min);
        nat = int.max(top_nat, bottom_nat);
    }

    protected override void get_preferred_height(out int min, out int nat) {
        int bottom_min = 0, bottom_nat = 0;
        int top_min = 0, top_nat = 0;

        if (bottom != null) {
            bottom.get_preferred_height(out bottom_min, out bottom_nat);
        }
        if (top != null) {
            top.get_preferred_height(out top_min, out top_nat);
        }

        min = bottom_min + top_min;
        nat = bottom_nat + top_nat;
    }


    protected override void size_allocate(Gtk.Allocation alloc) {
        double transition = tween[Tweens.TRANSITION];
        double top_transition = tween[Tweens.TOP_TRANSITION];

        if (side != null) {
            int min, nat;
            side.get_preferred_width(out min, out nat);

            int width = int.min(int.max(min, nat), alloc.width);

            side.size_allocate({
                (alloc.x + alloc.width) - (int) (width * (1 - transition)),
                alloc.y,
                width,
                alloc.height
            });

            int min_height;
            side.get_preferred_height(out min_height, null);
            if (alloc.width < MOBILE_LAYOUT_THRESHOLD || alloc.width < alloc.height || min_height > alloc.height) {
                if (transition == 0) {
                    sidebar_showing = false;
                    tween[Tweens.TRANSITION] = 1;
                }
            } else if (transition == 1) {
                sidebar_showing = true;
                tween[Tweens.TRANSITION] = 0;
            }

            side.set_child_visible(transition != 1);
        }

        if (bottom != null) {
            int min, nat;
            bottom.get_preferred_height(out min, out nat);

            int height = int.min(int.max(min, nat), alloc.height);

            bottom.size_allocate({
                alloc.x,
                (alloc.y + alloc.height) - (int) (height * transition),
                alloc.width,
                height
            });

            bottom.set_child_visible(transition != 0);
        }

        if (top != null) {
            int min, nat;
            top.get_preferred_height(out min, out nat);

            int height = int.min(int.max(min, nat), alloc.height);

            top.size_allocate({
                alloc.x,
                alloc.y - (int) (height * (1 - top_transition)),
                alloc.width,
                height
            });
        }

        base.size_allocate(alloc);
    }


    protected override void forall_internal(bool include_internals, Gtk.Callback callback) {
        if (side != null) {
            callback(side);
        }
        if (bottom != null) {
            callback(bottom);
        }
        if (top != null) {
            callback(top);
        }
    }

    protected override void remove(Gtk.Widget widget)
            requires (widget == side || widget == bottom || widget == top) {

        widget.unparent();

        if (widget == side) {
            side = null;
        } else if (widget == side) {
            bottom = null;
        } else if (widget == side) {
            top = null;
        }
    }


    private void on_layout_update() {
        if (bottom.narrow && !sidebar_showing) {
            tween[Tweens.TOP_TRANSITION] = 1;
        } else {
            tween[Tweens.TOP_TRANSITION] = 0;
        }
    }
}
